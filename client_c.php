<?php

use Marketplace\Connection\Client\ClientCustomer;

include __DIR__ . "/vendor/autoload.php";

try {
    $client = new ClientCustomer();
} catch (Exception $exception) {
    echo "Errore: " . $exception->getMessage() . PHP_EOL;
    exit(1);
}

$name = input("Negozio da cercare: ");
$shops = $client->retrieveShop($name);

print_r($shops);

$client->close();

function input($message = "")
{
    echo $message;
    $msg = fgets(STDIN);
    return str_replace(["\r", "\n"], "", trim($msg));
}
