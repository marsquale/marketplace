from app.connection.Server.ServerMerchant import ServerMerchant
from app.connection.Server.Threads.MerchantManager import MerchantManager
from threading import Thread, Lock

import sys

server_address = sys.argv[1] if len(sys.argv) > 1 else "localhost"

try:
    server = ServerMerchant.getInstance(server_address)
except Exception as e:
    print("Errore creazione server: %s" % str(e))
    exit(1)

mutex = Lock()
while True:
    client = server.accept()
    thread = MerchantManager(client, mutex)
    thread.start()

server.close()
