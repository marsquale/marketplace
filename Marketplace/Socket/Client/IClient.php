<?php

namespace Marketplace\Socket\Client;

interface IClient
{
    public function receive();
    public function send($message);
    public function close();
    public function isValid();
    public function getHost();
    public function getPort();
}