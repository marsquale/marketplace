<?php

namespace Marketplace\Socket\Client;

class ClientImpl implements IClient
{
    protected $_socket = null;
    protected $_chunk = 2048;
    protected $_info = array();

    public function __construct($socket = null)
    {
        $this->_socket = $socket;
        $this->initInfo();
    }

    public function init($host, $port)
    {
        $this->_socket = @socket_create(AF_INET, SOCK_STREAM, 0);

        echo "Connecting to: {$host}:{$port}";

        $result = @socket_connect($this->_socket, $host, $port);
        if ($result === false) {
            $this->raiseError();
        }

        $this->initInfo();
    }

    protected function initInfo()
    {
        $address = null;
        $port = null;
        $result = @socket_getpeername($this->_socket, $address, $port);
        if ($result === false) {
            $this->raiseError();
        }

        $this->_info["host"] = $address;
        $this->_info["port"] = $port;
    }

    public function receive()
    {
        if ($this->_socket === null || $this->_socket === false) {
            $this->raiseError();
        }

        $message = socket_read($this->_socket, $this->_chunk);
        return utf8_decode($message);
    }

    public function send($message)
    {
        if ($this->_socket === null || $this->_socket === false) {
            $this->raiseError();
        }

        $message = utf8_encode($message);
        @socket_write($this->_socket, $message, strlen($message));
    }

    private function raiseError()
    {
        if ($this->_socket) {
            $lastErrorCode = socket_last_error($this->_socket);
        } else {
            $lastErrorCode = socket_last_error();
        }

        $errorMessage = socket_strerror($lastErrorCode);
        throw new \Exception($errorMessage);
    }

    public function close()
    {
        @socket_close($this->_socket);
    }

    public function isValid()
    {
        return $this->_socket !== null || $this->_socket !== false;
    }

    public function getHost()
    {
        return $this->_info["host"];
    }

    public function getPort()
    {
        return $this->_info["port"];
    }
}