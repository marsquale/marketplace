<?php

namespace Marketplace\Connection\Client;

class ClientCustomer extends ClientGeneral
{
    public const SERVER_PORT = 9003;
    public const CONNECTION_SETTINGS_PATH = "client_settings.txt";

    public function retrieveShop($data)
    {
        $shopData = ["shop" => $data["query"]];
        $shopsJson = $this->retrieveData(self::SEARCH_SHOP_OPERATION, $shopData);

        return json_decode($shopsJson, true);
    }

    public function retrieveProduct($data)
    {
        $productQuery = $data["query"];
        $shop = $data["shop"];

        $productId = null;
        if (array_key_exists("product_id", $data)) {
            $productId = $data["product_id"];
        }

        if ($shop || $shop === "0") {
            $productData = [
                "product" => $productQuery,
                "shop" => $shop
            ];
        } else {
            $productData = [
                "product" => $productQuery,
            ];
        }

        if ($productId !== null) {
            $productData["id"] = $productId;
        }

        $productsJson = $this->retrieveData(self::SEARCH_PRODUCT_OPERATION, $productData);

        return json_decode($productsJson, true);
    }

    public function listShops()
    {
        $merchant = ["merchant" => "*"];
        $response = $this->retrieveData(self::LIST_SHOPS_OPERATION, $merchant);
        return json_decode($response, true);
    }

    public function switchOperation($method, $data)
    {
        switch ($method) {
            case self::SEARCH_SHOP_OPERATION:
                return $this->retrieveShop($data);
            case self::SEARCH_PRODUCT_OPERATION:
                return $this->retrieveProduct($data);
            default:
                return array("error" => $method . " non è un'operazione disponibile");
        }
    }
}
