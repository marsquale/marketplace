<?php

namespace Marketplace\Connection\Client;

use Marketplace\Socket\Client\ClientImpl;

class ClientGeneral extends ClientImpl
{
    public const SERVER_PORT = 9000;
    protected $_serverAddress = "";

    public const CONNECTION_SETTINGS_PATH = "";

    public const NEW_SHOP_OPERATION = "new_shop";
    public const REMOVE_SHOP_OPERATION = "remove_shop";
    public const NEW_PRODUCT_OPERATION = "new_product";
    public const REMOVE_PRODUCT_OPERATION = "remove_product";
    public const SEARCH_SHOP_OPERATION = "search_shop";
    public const SEARCH_PRODUCT_OPERATION = "search_product";
    public const LOGIN_OPERATION = "login";
    public const REGISTER_OPERATION = "register";
    public const LIST_SHOPS_OPERATION = "list_shops";
    public const LIST_PRODUCTS_OPERATION = "list_products";
    public const CLOSE_OPERATION = "close";

    public function __construct($socket = null)
    {
        $this->_serverAddress = $this->readConnectionSettings();
        $this->init($this->_serverAddress, static::SERVER_PORT);
        $this->initInfo();
    }

    private function readConnectionSettings()
    {
        $settingsPath = $_SERVER['DOCUMENT_ROOT'] . "/settings/" . static::CONNECTION_SETTINGS_PATH;
        $exists = file_exists($settingsPath);
        if ($exists === false) {
            $server = "localhost";
        } else {
            $server = file_get_contents($settingsPath);
        }

        return $server;
    }

    public function close()
    {
        $this->retrieveData(self::CLOSE_OPERATION, "1");
        parent::close();
    }

    public function retrieveData($method, $data)
    {
        $pack = [
            "method" => $method,
            "data" => $data
        ];

        $packToSend = json_encode($pack);

        echo "Inviando: \n";
        print_r($packToSend);
        $this->send($packToSend);

        return $this->receive();
    }
}
