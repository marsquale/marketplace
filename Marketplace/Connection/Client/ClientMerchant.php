<?php

namespace Marketplace\Connection\Client;

class ClientMerchant extends ClientGeneral
{
    public const SERVER_PORT = 9002;
    public const CONNECTION_SETTINGS_PATH = "merchant_settings.txt";

    public function createShop(array $shop)
    {
        $response = $this->retrieveData(self::NEW_SHOP_OPERATION, $shop);
        return json_decode($response, true);
    }

    public function createProduct(array $product)
    {
        $response = $this->retrieveData(self::NEW_PRODUCT_OPERATION, $product);
        return json_decode($response, true);
    }

    public function removeShop(array $shop)
    {
        $response = $this->retrieveData(self::REMOVE_SHOP_OPERATION, $shop);
        return json_decode($response, true);
    }

    public function removeProduct(array $product)
    {
        $response = $this->retrieveData(self::REMOVE_PRODUCT_OPERATION, $product);
        return json_decode($response, true);
    }

    public function listShops(array $shop)
    {
        $response = $this->retrieveData(self::LIST_SHOPS_OPERATION, $shop);
        return json_decode($response, true);
    }

    public function listProducts(array $product)
    {
        $response = $this->retrieveData(self::LIST_PRODUCTS_OPERATION, $product);
        return json_decode($response, true);
    }

    public function login($username = "", $password = "")
    {
        $pkg = ["merchant" => ["username" => $username, "password" => $password]];
        $response = $this->retrieveData(self::LOGIN_OPERATION, $pkg);
        return json_decode($response, true);
    }

    public function register($username = "", $password = "")
    {
        $pkg = ["merchant" => ["username" => $username, "password" => $password]];
        $response = $this->retrieveData(self::REGISTER_OPERATION, $pkg);
        return json_decode($response, true);
    }

    public function switchOperation($method, $data)
    {
        switch ($method) {
            case self::NEW_PRODUCT_OPERATION:
                return $this->createProduct($data);
            case self::NEW_SHOP_OPERATION:
                return $this->createShop($data);
            case self::REMOVE_SHOP_OPERATION:
                return $this->removeShop($data);
            case self::REMOVE_PRODUCT_OPERATION:
                return $this->removeProduct($data);
            default:
                return array("error" => $method . " non è un'operazione disponibile");
        }
    }
}
