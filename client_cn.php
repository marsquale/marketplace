<?php

use Marketplace\Connection\Client\ClientMerchant;

include __DIR__ . "/vendor/autoload.php";

try {
    $client = new ClientMerchant();
} catch (Exception $exception) {
    echo "Errore: " . $exception->getMessage() . PHP_EOL;
    exit(1);
}

$shop = input("Nome negozio: ");
$address = input("Indirizzo negozio: ");
$merchant = input("Negoziante: ");

$shopData = ["name" => $shop, "address" => $address, "merchant" => $merchant];
$response = $client->createShop($shopData);

print_r($response);

$client->close();

function input($message = "")
{
    echo $message;
    $msg = fgets(STDIN);
    return str_replace(["\r", "\n"], "", trim($msg));
}
