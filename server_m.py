from app.connection.Server.ServerWrapper import ServerWrapper
from app.connection.Server.Threads.WrapperManager import WrapperManager
from threading import Thread, Lock
import signal

try:
    server = ServerWrapper.getInstance()
    threads = []
except Exception as e:
    print("Errore creazione server: %s" % str(e))
    exit(1)    

def onCloseScript(signalName, frame):
    print("Sto chiudendo il server...")
    gracefullyClose()

def gracefullyClose():
    print("Attendo gli altri server...")
    for t in threads:
        t.join()
    
    print("Terminati threads")
    server.saveAll()
    server.close()
    exit(0)

signal.signal(signal.SIGINT, onCloseScript)
signal.signal(signal.SIGTERM, onCloseScript)

mutex = Lock()

while True:
    client = server.accept()
    thread = WrapperManager(client, mutex)
    threads.append(thread)

    thread.start()

print("Aspetto gli altri server...")
for t in threads:
    t.join()

server.saveAll()
server.close()
