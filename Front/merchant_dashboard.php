<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

use Marketplace\Connection\Client\ClientMerchant;
include_once $_SERVER["DOCUMENT_ROOT"] . "/../vendor/autoload.php";
include_once "templates/MerchantDashboard.php";

if (!isset($_SESSION["login"]) || $_SESSION["login"] !== true) {
    header("Location: /merchant_account.php"); die();
}

try {
    $client = new ClientMerchant();

    $file = __DIR__ . "/templates/merchant/dashboard.phtml";
    $template = new MerchantDashboard($file);
} catch (Exception $exception) {
    echo "Errore: " . $exception->getMessage() . "<br>";
    exit(1);
}

// Se ho submittato uno dei 4 form
if (isset($_POST["submit"])) {
    $method = $_POST["method"];
    $data = $_POST["data"];
    $merchant = $_SESSION["username"];

    $data["merchant"] = $merchant;

    $response = $client->switchOperation($method, $data);
    $template->setResult($response);
}

$pkg = ["merchant" => $_SESSION["username"]];
$shops = $client->listShops($pkg);
//print_r($shops);
$products = $client->listProducts($pkg);
//print_r($products);

$template->setShops($shops["data"]);
$template->setProducts($products["data"]);

echo $template->render();

$client->close();