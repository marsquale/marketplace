<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Marketplace\Connection\Client\ClientCustomer;
include_once $_SERVER["DOCUMENT_ROOT"] . "/../vendor/autoload.php";
include_once "templates/ClientResult.php";

try {
    $client = new ClientCustomer();

    $file = __DIR__ . "/templates/client/result.phtml";
    $template = new ClientResult($file);
} catch (Exception $exception) {
    echo "Errore: " . $exception->getMessage();
    exit(0);
}

$method = $_POST["method"];
$query = $_POST["query"];
$shop = null;

if (array_key_exists("shop", $_POST)) {
    $shop = $_POST["shop"];
}

$result = $client->switchOperation($method, ["query" => $query, "shop" => $shop]);
$template->setResult($result);

echo $template->render();

$client->close();