<?php

include_once __DIR__ . "/Template.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/../vendor/autoload.php";

class MerchantAccount extends Template
{
    protected $_result = array();

    public function setResult($result)
    {
        $this->_result = $result;
    }

    public function getResult()
    {
        return $this->_result;
    }

    public function getErrorMessage()
    {
        $result = $this->getResult();
        if (!$result || empty($result)) {
            return "";
        }

        if (!array_key_exists("error", $result)) {
            return "";
        }

        return (string) $result["error"];
    }
}
