<?php

include_once __DIR__ . "/Template.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/../vendor/autoload.php";

class MerchantDashboard extends Template
{
    protected $_result = array();
    protected $_shops = array();
    protected $_products = array();

    public function setResult($result)
    {
        $this->_result = $result;
    }

    public function getResult()
    {
        return $this->_result;
    }

    public function getData()
    {
        $result = $this->getResult();
        return $result["data"];
    }

    public function isSuccess()
    {
        $result = $this->getResult();
        if (!$result || empty($result)) {
            return false;
        }

        if (!array_key_exists("success", $result)) {
            return false;
        }

        return (bool) $result["success"];
    }

    public function getMessageType()
    {
        return ($this->isSuccess())? "success" : "danger";
    }

    public function getMessage()
    {
        $result = $this->getResult();
        if ($this->isSuccess()) {
            $msg = $result["data"];
        } else if (array_key_exists("error", $result)) {
            $msg = $result["error"];
        } else {
            $msg = "";
        }

        return $msg;
    }

    public function setShops($shops)
    {
        $this->_shops = $shops;
    }

    public function getShops()
    {
        return $this->_shops;
    }

    public function setProducts($products)
    {
        $this->_products = $products;
    }

    public function getProducts()
    {
        return $this->_products;
    }

    public function getMerchantUsername()
    {
        return $_SESSION["username"];
    }
}
