<?php

include_once "Template.php";

class ClientCart extends Template
{
    protected $_error = null;
    protected $_success = null;
    protected $_info = null;
    protected $_products = array();
    protected $_productToAdd = null;

    public function setProducts($products)
    {
        $this->_products = $products;
    }

    public function getProducts()
    {
        return $this->_products;
    }

    public function setError($error)
    {
        $this->_error = $error;
    }

    public function getError()
    {
        return $this->_error;
    }

    public function setSuccess($success)
    {
        $this->_success = $success;
    }

    public function getSuccess()
    {
        return $this->_success;
    }

    public function setInfo($info)
    {
        $this->_info = $info;
    }

    public function getInfo()
    {
        return $this->_info;
    }

    public function setProductToAdd($product)
    {
        $this->_productToAdd = $product;
    }

    public function getProductToAdd()
    {
        return $this->_productToAdd;
    }

    public function getDefaultImage()
    {
        return "https://picsum.photos/275/150";
    }

    public function addToCart()
    {
        $products = $this->getProducts();
        if (!$products) {
            $products = [];
        }

        if (!$this->getProductToAdd()) {
            return;
        }

        $product = $this->getProductToAdd();
        if (!$this->shouldAddProduct($product)) {
            $this->setInfo("Prodotto già aggiunto!");
            return;
        }

        $products[] = $product;
        $this->setSuccess("Il prodotto è stato aggiunto correttamente!");
        $this->setProducts($products);
        $_SESSION["products"] = $this->getProducts();
    }

    private function shouldAddProduct($productToAdd)
    {
        foreach ($this->getProducts() as $product) {
            if ($product["product"]["id"] == $productToAdd["product"]["id"]) {
                return false;
            }
        }

        return true;
    }
}
