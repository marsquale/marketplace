<?php

include_once __DIR__ . "/Template.php";
use Marketplace\Connection\Client\ClientCustomer;
include_once $_SERVER["DOCUMENT_ROOT"] . "/../vendor/autoload.php";

class ClientResult extends Template
{
    protected $_result = array();

    public function setResult($result)
    {
        $this->_result = $result;
    }

    public function getResult()
    {
        return $this->_result;
    }

    public function isValid()
    {
        $result = $this->getResult();
        if (!$result || empty($result) || !array_key_exists("success", $result)) { return false; }

        return (bool) $result["success"];
    }

    public function isShopSearch()
    {
        if (!$this->isValid()) { return false; }
        return $_POST["method"] == ClientCustomer::SEARCH_SHOP_OPERATION;
    }

    public function isProductSearch()
    {
        if (!$this->isValid()) { return false; }
        return $_POST["method"] == ClientCustomer::SEARCH_PRODUCT_OPERATION;
    }

    public function getData()
    {
        if (!$this->isValid()) { return array(); }
        $result = $this->getResult();

        return $result["data"];
    }

    public function getTitle()
    {
        $title = "";
        if ($this->isProductSearch()) {
            $title = "Prodotti trovati con ricerca: " . $_POST["query"];
        } else {
            $title = "Negozi trovati con ricerca: " . $_POST["query"];
        }

        return $title;
    }

    public function getDefaultImage()
    {
        return "https://picsum.photos/275/150";
    }
}