<?php

include_once __DIR__ . "/Template.php";
use Marketplace\Connection\Client\ClientCustomer;
include_once $_SERVER["DOCUMENT_ROOT"] . "/../vendor/autoload.php";

class ClientDashboard extends Template
{
    protected $_shops = array();

    public function setShops($shops)
    {
        $this->_shops = $shops;
    }

    public function getShops()
    {
        return $this->_shops;
    }
}
