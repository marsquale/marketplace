<?php

class Template
{
    protected $_template = "";

    public function __construct($_template)
    {
        $this->_template = $_template;
    }

    public function getTemplate()
    {
        return $this->_template;
    }

    public function render()
    {
        if (!$this->getTemplate() || empty($this->getTemplate())) {
            return "";
        }

        ob_start();
        $template = $this->getTemplate();
        include_once $template;
        $fileContent = ob_get_contents();
        ob_end_clean();

        return $fileContent;
    }
}