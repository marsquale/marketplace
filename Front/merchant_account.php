<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
use Marketplace\Connection\Client\ClientMerchant;
include_once $_SERVER["DOCUMENT_ROOT"] . "/../vendor/autoload.php";
include_once "templates/MerchantAccount.php";

try {
    $client = new ClientMerchant();

    $file = __DIR__ . "/templates/merchant/account.phtml";
    $template = new MerchantAccount($file);

} catch (Exception $exception) {
    echo "Errore: " . $exception->getMessage() . "<br>";
    exit(1);
}

$errorMsg = "";
$validUser = (isset($_SESSION["login"]))? ($_SESSION === true) : false;
if(isset($_POST["submit"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];

    if ($_POST["method"] == "login") {
        $response = $client->login($username, $password);
    } else {
        $response = $client->register($username, $password);
    }

    $client->close();
    $validUser = (array_key_exists("success", $response))? (bool)$response["success"] : false;

    if(!$validUser) $errorMsg = $response["error"];
    else {
        $_SESSION["login"] = true;
        $_SESSION["username"] = $username;
    }

    $template->setResult($response);
}

if($validUser) {
    header("Location: /merchant_dashboard.php"); die();
}

echo $template->render();