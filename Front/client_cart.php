<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
use Marketplace\Connection\Client\ClientCustomer;
include_once $_SERVER["DOCUMENT_ROOT"] . "/../vendor/autoload.php";
include "templates/ClientCart.php";

if (array_key_exists("products", $_SESSION)) {
    $products = $_SESSION["products"];
} else {
    $products = [];
}

try {
    $client = new ClientCustomer();

    $file = __DIR__ . "/templates/client/cart.phtml";
    $template = new ClientCart($file);

    $template->setProducts($products);

} catch (Exception $exception) {
    echo "Errore: " . $exception->getMessage();
    exit(0);
}

if (array_key_exists("product_id", $_GET)) {
    $productId = $_GET["product_id"];
} else {
    $productId = null;
}

if ($productId !== null) {
    $result = $client->retrieveProduct(["query" => "", "shop" => "", "product_id" => $productId]);
    if (array_key_exists("error", $result)) {
        $template->setError($result["error"]);
    } else {
        $products = $result["data"];
        if (count($products) <= 0) {
            $template->setError("Nessun prodotto trovato con id {$productId}");
        } else {
            $template->setProductToAdd($products[0]);
        }
    }

    $template->addToCart();
}

echo $template->render();
