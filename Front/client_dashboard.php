<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

use Marketplace\Connection\Client\ClientCustomer;
include_once $_SERVER["DOCUMENT_ROOT"] . "/../vendor/autoload.php";
include "templates/ClientDashboard.php";
$template = __DIR__ . "/templates/client/dashboard.phtml";

$client = new ClientCustomer();

$shops = $client->listShops();
$dashboard = new ClientDashboard($template);

$dashboard->setShops($shops["data"]);
echo $dashboard->render();

