from .IClient import IClient
import socket

class ClientImpl(IClient):
    def __init__(self, socket=None, info=None):
        self.socket = socket
        self.chunk = 2048

        if (info is not None):
            self.info = {"host": info[0], "port": info[1]}
    
    def initialize(self, host, port):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))
        self.initializeInfo()
    
    def initializeInfo(self):
        result = self.socket.getpeername()
        self.info = {"host": result[0], "port": result[1]}
    
    def getChunk(self):
        return self.chunk
    
    def getHost(self):
        return self.info["host"]
    
    def getPort(self):
        return self.info["port"]
    
    def receive(self):
        data = self.socket.recv(self.getChunk())
        return data.decode('utf-8')
    
    def send(self, message):
        self.socket.send(message.encode('utf-8'))

    def close(self):
        self.socket.close()
    
    def isValid(self):
        return self.socket != None

