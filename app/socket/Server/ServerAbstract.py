from app.socket.Client.ClientImpl import ClientImpl
import socket
import json

class ServerAbstract(object):
    NEW_SHOP_OPERATION = "new_shop"
    REMOVE_SHOP_OPERATION = "remove_shop"
    NEW_PRODUCT_OPERATION = "new_product"
    REMOVE_PRODUCT_OPERATION = "remove_product"
    SEARCH_SHOP_OPERATION = "search_shop"
    SEARCH_PRODUCT_OPERATION = "search_product"
    LOGIN_OPERATION = "login"
    REGISTER_OPERATION = "register"
    LIST_SHOPS = "list_shops"
    LIST_PRODUCTS = "list_products"
    CLOSE_OPERATION = "close"

    choices = [
        NEW_SHOP_OPERATION, 
        REMOVE_SHOP_OPERATION,
        NEW_PRODUCT_OPERATION,
        REMOVE_PRODUCT_OPERATION,
        SEARCH_SHOP_OPERATION,
        SEARCH_PRODUCT_OPERATION,
        LOGIN_OPERATION,
        REGISTER_OPERATION,
        LIST_PRODUCTS,
        LIST_SHOPS,
        CLOSE_OPERATION
    ]

    def __init__(self):
        self.host = "0.0.0.0"
        self.port = 3000
        self.backlog = 5
        self.clients = []
        self.socket = None

    def getHost(self):
        return self.host

    def getPort(self):
        return self.port

    def getBacklog(self):
        return self.backlog

    def getSocket(self):
        return self.socket

    def initialize(self):
        print("Listening on %s:%s" % (self.getHost(), self.getPort()))
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.getHost(), self.getPort()))
        self.socket.listen(self.getBacklog())

    def accept(self):
        (client, address) = self.socket.accept()
        clientObj = ClientImpl(client, address)
        self.clients.append(clientObj)

        return clientObj

    def close(self):
        for client in self.clients:
            client.close()

        self.socket.close()

    def parseClientMessage(self, message):
        messageParsed = json.loads(message)
        print(messageParsed)
        if ("method" not in messageParsed):
            raise Exception("Operazione non valida\n")

        if (self.isValidOperation(messageParsed["method"]) == False):
            raise Exception("Operazione %s non valida" % (messageParsed["method"]))

        return messageParsed

    def isValidOperation(self, operation):
        return operation in ServerAbstract.choices