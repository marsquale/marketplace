from .IShop import IShop
from app.model.Object import Object

class Shop(Object, IShop):
    fields_name = ["id", "name", "address", "merchant_id"]

    def setId(self, id):
        self.setData("id", id)

    def getId(self):
        return int(self.getData("id"))

    def setName(self, name):
        self.setData("name", name)

    def getName(self):
        return self.getData("name")

    def setAddress(self, address):
        self.setData("address", address)

    def getAddress(self):
        return self.getData("address")

    def setMerchantId(self, merchantId):
        self.setData("merchant_id", merchantId)

    def getMerchantId(self):
        return int(self.getData("merchant_id"))

    @classmethod
    def getFieldsName(cls):
        return cls.fields_name
