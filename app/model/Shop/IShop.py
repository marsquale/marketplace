from app.model.IEntity import IEntity

class IShop(IEntity):
    def setId(self, id):
        pass
    def getId(self):
        pass
    def setName(self, name):
        pass
    def getName(self):
        pass
    def setAddress(self, address):
        pass
    def getAddress(self):
        pass
    def setMerchantId(self, merchantId):
        pass
    def getMerchantId(self):
        pass
    @classmethod
    def getFieldName(cls):
        pass