from app.model.IFactory import IFactory
from .Shop import Shop

class ShopFactory(IFactory):
    @classmethod
    def make(cls, data):
        shop = Shop()

        id = -1 if data.get("id") is None else data.get("id")
        shop.setId(id)
        shop.setName(data.get("name"))
        shop.setAddress(data.get("address"))
        shop.setMerchantId(data.get("merchant_id"))

        return shop
