from app.model.IEntity import IEntity

class Object(IEntity):
    def __init__(self):
        self.data = {}
    def setData(self, key, value):
        self.data[key] = value
    def getData(self, key=None):
        if key is None:
            return self.data
        return self.data.get(key)
    def toArray(self):
        return self.getData().values()
    
    def toDict(self):
        return self.getData()
