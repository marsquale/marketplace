from app.model.IEntity import IEntity

class IProduct(IEntity):
    def setId(self, id):
        pass
    def getId(self):
        pass
    def setName(self, name):
        pass
    def getName(self):
        pass
    def setDescription(self, description):
        pass
    def getDescription(self):
        pass
    def setPrice(self, price):
        pass
    def getPrice(self):
        pass
    def setQty(self, qty):
        pass
    def getQty(self, qty):
        pass
    def setShopId(self, shopId):
        pass
    def getShopId(self):
        pass
    def getImage(self):
        pass
    def setImage(self, image):
        pass

    @classmethod
    def getFieldsName(cls):
        pass