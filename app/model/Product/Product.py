from .IProduct import IProduct
from app.model.Object import Object

class Product(Object, IProduct):
    fields_name = ["id", "name", "description", "price", "qty", "shop_id", "image"]

    def setId(self, id):
        self.setData("id", id)
    def getId(self):
        return int(self.getData("id"))
    def setName(self, name):
        self.setData("name", name)
    def getName(self):
        return self.getData("name")
    def setDescription(self, description):
        self.setData("description", description)
    def getDescription(self):
        return self.getData("description")
    def setPrice(self, price):
        self.setData("price", price)
    def getPrice(self):
        return float(self.getData("price"))
    def setQty(self, qty):
        self.setData("qty", qty)
    def getQty(self):
        return int(self.getData("qty"))
    def setShopId(self, shopId):
        self.setData("shop_id", shopId)
    def getShopId(self):
        return int(self.getData("shop_id"))
    def getImage(self):
        return self.getData("image")
    def setImage(self, image):
        self.setData("image", image)

    @classmethod
    def getFieldsName(cls):
        return cls.fields_name