from app.model.IFactory import IFactory
from .Product import Product

class ProductFactory(IFactory):
    @classmethod
    def make(cls, data):
        product = Product()

        id = -1 if data.get("id") is None else data.get("id")
        product.setId(id)
        product.setName(data.get("name"))
        product.setDescription(data.get("description"))
        product.setPrice(data.get("price"))
        product.setQty(data.get("qty"))
        product.setShopId(data.get("shop_id"))
        product.setImage(data.get("image"))

        return product

