from .IMerchant import IMerchant
from app.model.Object import Object

class Merchant(Object, IMerchant):
    fields_name = ["id", "username", "password"]

    def setId(self, id):
        self.setData("id", id)
    def getId(self):
        return int(self.getData("id"))
    def setUsername(self, username):
        self.setData("username", username)
    def getUsername(self):
        return self.getData("username")
    def setPassword(self, password):
        self.setData("password", password)
    def getPassword(self):
        return self.getData("password")
    def toDict(self):
        data = self.getData().copy()
        del data["password"]        
        return data
        
    @classmethod
    def getFieldsName(cls):
        return cls.fields_name
