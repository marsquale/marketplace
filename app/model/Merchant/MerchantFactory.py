from .Merchant import Merchant
from app.model.IFactory import IFactory

class MerchantFactory(IFactory):
    @classmethod
    def make(self, data):
        merchant = Merchant()

        id = -1 if data.get("id") is None else data.get("id")
        merchant.setId(id)
        merchant.setUsername(data.get("username"))
        merchant.setPassword(data.get("password"))

        return merchant