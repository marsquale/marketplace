from .IRepository import IRepository
from app.model.Merchant.Merchant import Merchant
from app.model.Merchant.MerchantFactory import MerchantFactory

class MerchantRepository(object):
    instance = None

    class __MerchantRepository(IRepository):
        file = "merchants.csv"

        def rowToObject(self, row):
            merchant = {}
            fields = Merchant.getFieldsName()
            for i in range(len(fields)):
                merchant[fields[i]] = row[i]

            return MerchantFactory.make(merchant)
    
    @classmethod
    def getInstance(cls):
        if (cls.instance != None):
            return cls.instance
        cls.instance = MerchantRepository.__MerchantRepository()
        return cls.instance
