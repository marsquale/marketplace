from .IRepository import IRepository
from app.model.Product.Product import Product
from app.model.Product.ProductFactory import ProductFactory

class ProductRepository(object):
    instance = None

    class __ProductRepository(IRepository):
        file = "products.csv"

        def rowToObject(self, row):
            product = {}
            fields = Product.getFieldsName()
            for i in range(len(fields)):
                product[fields[i]] = row[i]

            return ProductFactory.make(product)

    @classmethod
    def getInstance(cls):
        if (cls.instance != None):
            return cls.instance
        cls.instance = ProductRepository.__ProductRepository()
        return cls.instance
        