from .IRepository import IRepository
from app.model.Shop.Shop import Shop
from app.model.Shop.ShopFactory import ShopFactory

class ShopRepository(object):
    instance = None

    class __ShopRepository(IRepository):
        file = "shops.csv"

        def rowToObject(self, row):
            shop = {}
            fields = Shop.getFieldsName()
            for i in range(len(fields)):
                shop[fields[i]] = row[i]

            return ShopFactory.make(shop)


    
    @classmethod
    def getInstance(cls):
        if (cls.instance != None):
            return cls.instance
        cls.instance = ShopRepository.__ShopRepository()
        return cls.instance