import sys
import csv
from app.helpers.FileHelper import FileHelper


class IRepository(object):
    mediaDir = "media"
    file = ""
    
    def __init__(self):
        self.lastId = 0
        self.objects = []
        self._initFileName()
        self._readObjects()
                
    def _readObjects(self):
        FileHelper.createFileIfNotExists(self.fileName)

        with open(self.fileName, "r") as handler:
            csvHandler = csv.reader(handler, delimiter=",")
            for row in csvHandler:
                obj = self.rowToObject(row)
                self.add(obj)
    
    def rowToObject(self, row):
        pass

    def get(self, id):
        try:
            return self.find(id)
        except IndexError:
            return None
        
    def getAll(self):
        return self.objects
    
    def getByAttribute(self, attribute, value):
        found = []

        for obj in self.objects:
            if (str(obj.getData(attribute)).strip() == value.strip()):
                found.append(obj)
        
        return found
        
    def save(self):
        with open(self.fileName, "w") as handler:
            csvWriter = csv.writer(handler, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for obj in self.objects:
                csvWriter.writerow(obj.toArray())

    def add(self, obj):
        if obj.getId() != None and obj.getId() >= 0:
            self.lastId = obj.getId()

        obj.setId(self.lastId)
        self.objects.insert(obj.getId(), obj)
        self.lastId = self.lastId + 1

    def remove(self, obj):
        idx = self.findIndex(obj.getId())
        del self.objects[idx]

    def _initFileName(self):
        filePattern = "{base}/{media}/{file}"
        self.fileName = filePattern.format(base=sys.path[0], media=type(self).mediaDir, file=type(self).file)

    def find(self, id):
        for el in self.objects:
            if el.getId() == id:
                return el
        return None

    def findIndex(self, id):
        for i in range(0, len(self.objects)):
            if id == self.objects[i].getId():
                return i
        return -1