from app.model.Storage.ShopRepository import ShopRepository

class ShopManagement(object):
    def __init__(self):
        self.shopRepository = ShopRepository.getInstance()
    
    def getById(self, id):
        return self.shopRepository.get(id)
    
    def getByName(self, name):
        shops = self.shopRepository.getAll()
        found = []

        for shop in shops:
            shopName = shop.getName().lower().strip()
            nameToFind = name.lower().strip()

            if nameToFind in shopName:
                found.append(shop)
        
        return found

    def saveChanges(self):
        self.shopRepository.save()

    def getList(self):
        return self.shopRepository.getAll()
    
    def getShopsByMerchantId(self, merchantId):
        p = self.shopRepository.getByAttribute("merchant_id", str(merchantId))
        return p

    def addShop(self, shop):
        self.shopRepository.add(shop)
    
    def removeShop(self, shop):
        self.shopRepository.remove(shop)