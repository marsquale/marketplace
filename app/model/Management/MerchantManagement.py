from app.model.Storage.MerchantRepository import MerchantRepository

class MerchantManagement(object):
    def __init__(self):
        self.merchantRepository = MerchantRepository.getInstance()
    
    def saveChanges(self):
        self.merchantRepository.save()

    def getList(self):
        return self.merchantRepository.getAll()

    def getById(self, id):
        return self.merchantRepository.get(id)
    
    def getByUsername(self, username):
        merchants = self.merchantRepository.getByAttribute("username", username)
        if not merchants:
            return None

        return merchants[0]

    def login(self, username, password):
        merchant = self.getByUsername(username)
        if merchant is None:
            raise Exception("Nessun negoziante trovato con username %s" % (username))

        if merchant.getPassword() == password:
            return merchant
        
        raise Exception("Password errata")

    def addMerchant(self, merchant):
        found = self.getByUsername(merchant.getUsername())
        if found is not None:
            raise Exception("Negoziante con username %s già esistente!" % (merchant.getUsername()))

        self.merchantRepository.add(merchant)

    def removeMerchant(self, merchant):
        self.merchantRepository.remove(merchant)