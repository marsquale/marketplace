from app.model.Storage.ProductRepository import ProductRepository
from app.model.Storage.ShopRepository import ShopRepository

class ProductManagement(object):
    def __init__(self):
        self.productRepository = ProductRepository.getInstance()
        self.shopRepository = ShopRepository.getInstance()

    def saveChanges(self):
        self.productRepository.save()

    def getById(self, id):
        return self.productRepository.get(int(id))

    def getByName(self, name):
        found = []
        products = self.productRepository.getAll()

        for product in products:
            productName = product.getName().lower().strip()
            name = name.lower().strip()

            if name in productName:
                found.append(product)

        return found

    def findInShop(self, shopId):
        return self.productRepository.getByAttribute("shop_id", shopId)

    def findByMerchantId(self, merchantId):
        products = self.getList()
        productsToReturn = []

        for product in products:
            shop = self.shopRepository.get(int(product.getShopId()))
            if shop is None:
                continue

            if shop.getMerchantId() == merchantId:
                productsToReturn.append(product)

        return productsToReturn

    def findInShopByName(self, productName, shopId):
        products = self.findInShop(shopId)
        name = productName.lower().strip()

        for product in products:
            pName = product.getName().lower().strip()

            if name in pName:
                return product
        
        return None
    
    def connect(self, product, shop):
        product.setShopId(shop.getId())
    
    def addProductToShop(self, product, shop):
        self.connect(product, shop)
        self.productRepository.add(product)
    
    def removeProduct(self, product):
        self.productRepository.remove(product)
    
    def getList(self):
        return self.productRepository.getAll()