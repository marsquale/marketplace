from app.socket.Server.ServerAbstract import ServerAbstract
from app.model.Management.ProductManagement import ProductManagement
from app.model.Management.MerchantManagement import MerchantManagement
from app.model.Management.ShopManagement import ShopManagement
from app.model.Product.ProductFactory import ProductFactory
from app.model.Shop.ShopFactory import ShopFactory
from app.model.Merchant.MerchantFactory import MerchantFactory
from .CloseException import CloseException

import json

class ServerWrapper(object):
    instance = None

    @classmethod
    def getInstance(cls):
        if cls.instance != None:
            return cls.instance
        cls.instance = ServerWrapper.__ServerWrapper()
        return cls.instance

    class __ServerWrapper(ServerAbstract):
        def __init__(self):
            super(ServerWrapper.__ServerWrapper, self).__init__()

            self.port = 9000
            self.productManagement = ProductManagement()
            self.merchantManagement = MerchantManagement()
            self.shopManagement = ShopManagement()
            self.initialize()

        def getChoices(self):
            return __ServerWrapper.choices

        def findShop(self, clientData):
            shopName = clientData["shop"]
            shops = self.shopManagement.getByName(shopName)
            shopsToReturn = []

            for shop in shops:
                tempShop = {}
                tempShop["shop"] = shop.toDict()
                merchant = self.merchantManagement.getById(shop.getMerchantId())
                tempShop["merchant"] = merchant.toDict() if merchant != None else None

                shopsToReturn.append(tempShop)
            
            return shopsToReturn
        
        def findProduct(self, clientData):
            productName = clientData["product"] if "product" in clientData else None
            productId = clientData["id"] if "id" in clientData else None
            shopId = clientData["shop"] if "shop" in clientData else None

            if productId is not None:
                product = self.productManagement.getById(int(productId))
                products = [product]
                return self.buildProducts(products)

            if shopId is not None:
                if productName is None or productName.strip() == "":
                    products = self.productManagement.findInShop(shopId)
                else:
                    products = self.productManagement.findInShopByName(productName, shopId)
            else:
                products = self.productManagement.getByName(productName)

            return self.buildProducts(products)

        def buildProducts(self, products):
            productsToReturn = []
            for product in products:
                tempProduct = {}
                tempProduct["product"] = product.toDict()
                shop = self.shopManagement.getById(product.getShopId())

                tempProduct["shop"] = shop.toDict() if shop != None else None
                productsToReturn.append(tempProduct)

            return productsToReturn

        
        def createShop(self, shopJson):
            merchant = self.merchantManagement.getByUsername(shopJson["merchant"])
            if merchant is None:
                raise Exception("Nessun negoziante trovato con username %s" % (shopJson["merchant"]))

            shopJson["merchant_id"] = merchant.getId()
            shop = ShopFactory.make(shopJson)
            self.shopManagement.addShop(shop)

            message = "Negozio creato correttamente"
            return message

        def removeShop(self, shopJson):
            print("Removing shop")
            shop = self.retrieveShopFromClientData(shopJson)

            print("shop name: %s %s" % (shop.getName(), shop.getId()))
            self.shopManagement.removeShop(shop)

            message = "Negozio rimosso correttamente"
            return message
        
        def createProduct(self, productJson):
            shop = self.retrieveShopFromClientData(productJson)
            product = ProductFactory.make(productJson)

            self.productManagement.addProductToShop(product, shop)

            message = "Prodotto creato correttamente"
            return message
        
        def removeProduct(self, productJson):
            shop = self.retrieveShopFromClientData(productJson)
            product = self.productManagement.findInShopByName(productJson["product"], str(shop.getId()))

            if product is None:
                raise Exception("Prodotto %s non trovato" % (productJson["product"]))

            self.productManagement.removeProduct(product)

            message = "Prodotto rimosso correttamente"
            return message
        
        def login(self, merchantJson):
            if "merchant" not in merchantJson:
                raise Exception("Mancano i dati del negoziante per il login")

            username = merchantJson["merchant"]["username"]
            password = merchantJson["merchant"]["password"]

            merchant = self.merchantManagement.login(username, password)
            
            message = "Utente loggato"
            return message
        
        def register(self, merchantJson):
            if "merchant" not in merchantJson:
                raise Exception("Dati negoziante mancanti")

            merchant = MerchantFactory.make(merchantJson["merchant"])
            self.merchantManagement.addMerchant(merchant)
            message = "Negoziante %s registrato correttamente" % (merchant.getUsername())
            
            return message

        def listShops(self, shopJson):
            print(shopJson)
            if "merchant" not in shopJson:
                raise Exception("Dati negoziante mancanti")

            if shopJson["merchant"] == "*":
                print("Sono qui")
                shops = self.shopManagement.getList()
                return self.buildShopJson(shops)

            merchant = self.merchantManagement.getByUsername(shopJson["merchant"])
            if merchant is None:
                raise Exception("Negoziante con username %s non trovato" % (shopJson["merchant"]))

            shops = self.shopManagement.getShopsByMerchantId(merchant.getId())
            return self.buildShopJson(shops)

        def buildShopJson(self, shops):
            shopsToReturn = []

            for shop in shops:
                tempShop = {}
                tempShop["shop"] = shop.toDict()
                merchant = self.merchantManagement.getById(shop.getMerchantId())
                tempShop["merchant"] = merchant.toDict() if merchant != None else None

                shopsToReturn.append(tempShop)

            return shopsToReturn


        def listProducts(self, productJson):
            if "merchant" not in productJson:
                raise Exception("Dati negoziante mancanti")

            merchant = self.merchantManagement.getByUsername(productJson["merchant"])
            if merchant is None:
                raise Exception("Negoziante con username %s non trovato" % (productJson["merchant"]))

            products = self.productManagement.findByMerchantId(merchant.getId())
            productsToReturn = []

            for product in products:
                tempProduct = {}
                tempProduct["product"] = product.toDict()
                shop = self.shopManagement.getById(product.getShopId())

                tempProduct["shop"] = shop.toDict() if shop != None else None
                productsToReturn.append(tempProduct)

            return productsToReturn


        def retrieveShopFromClientData(self, clientData):
            if "merchant" not in clientData or "shop" not in clientData:
                raise Exception("Mancano i dati del negoziante o del negozio")

            merchant = self.merchantManagement.getByUsername(clientData["merchant"])
            if merchant is None:
                raise Exception("Nessun negoziante trovato con username %s" % (clientData["merchant"]))

            shops = self.shopManagement.getByName(clientData["shop"])
            for shop in shops:
                if shop.getMerchantId() == merchant.getId():
                    return shop

            raise Exception("Il negozio %s non appartiene al negoziante %s" % (clientData["shop"], merchant.getUsername()))

        def saveAll(self):
            self.shopManagement.saveChanges()
            self.productManagement.saveChanges()
            self.merchantManagement.saveChanges()

        def closeConnection(self):
            raise CloseException()

        def performOperation(self, client, clientMessage):
            clientData = self.parseClientMessage(clientMessage)
            result = self.switchOperation(clientData)

            package = {
                "success": True,
                "data": result
            }

            packageToSend = json.dumps(package)
            client.send(packageToSend)

        def switchOperation(self, clientData):
            switch = {
                self.NEW_SHOP_OPERATION: self.createShop,
                self.NEW_PRODUCT_OPERATION: self.createProduct,
                self.REMOVE_SHOP_OPERATION: self.removeShop,
                self.REMOVE_PRODUCT_OPERATION: self.removeProduct,
                self.SEARCH_SHOP_OPERATION: self.findShop,
                self.SEARCH_PRODUCT_OPERATION: self.findProduct,
                self.LOGIN_OPERATION: self.login,
                self.REGISTER_OPERATION: self.register,
                self.LIST_SHOPS: self.listShops,
                self.LIST_PRODUCTS: self.listProducts,
                self.CLOSE_OPERATION: self.closeConnection
            }

            func = switch.get(clientData["method"], lambda: "Operazione non valida")
            return func(clientData["data"])