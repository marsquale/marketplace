from threading import Thread
from app.connection.Server.ServerMerchant import ServerMerchant
from app.connection.Server.CloseException import CloseException
import json

class MerchantManager(Thread):
    def __init__(self, client, mutex):
        Thread.__init__(self)
        self.server = ServerMerchant.getInstance()
        self.client = client
        self.mutex = mutex
    
    def manager(self):
        print("Client connesso: %s:%s" % (self.client.getHost(), self.client.getPort()))

        quit = False
        while (not quit):
            message = self.client.receive()

            with self.mutex:
                try:
                    self.server.performOperation(self.client, message)
                except CloseException:
                    self.client.close()
                    quit = True
                except Exception as e:
                    error = {"error": str(e) + " server n"}
                    errorJson = json.dumps(error)
                    self.client.send(errorJson)

    def run(self):
        try:
            self.manager()
        except BrokenPipeError as e:
            print("Client disconnected. Quitting...")
            exit(0)