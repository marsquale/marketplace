from threading import Thread, Lock
from app.connection.Server.ServerWrapper import ServerWrapper
from app.connection.Server.CloseException import CloseException
import json

class WrapperManager(Thread):
    def __init__(self, client, mutex):
        Thread.__init__(self)
        self.server = ServerWrapper.getInstance()
        self.client = client
        self.mutex = mutex

    def manager(self):
        print("Client connesso: %s:%s" % (self.client.getHost(), self.client.getPort()))

        quit = False
        while (not quit):
            message = self.client.receive()

            with self.mutex:
                try:
                    self.server.performOperation(self.client, message)
                except CloseException:
                    self.client.close()
                    quit = True
                except Exception as e:
                    error = {"error": str(e) + " server m"}
                    errorJson = json.dumps(error)
                    self.client.send(errorJson)
        
        self.server.saveAll()
    
    def run(self):
        try:
            self.manager()
        except BrokenPipeError as e:
            print("Client disconnected. Exiting...")
            exit(0)
