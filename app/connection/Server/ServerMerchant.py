from app.socket.Server.ServerAbstract import ServerAbstract
from app.socket.Client.ClientImpl import ClientImpl
from .CloseException import CloseException

import json


class ServerMerchant(object):
    instance = None

    @classmethod
    def getInstance(cls, server_address="localhost"):
        if cls.instance is not None:
            return cls.instance
        
        cls.instance = ServerMerchant.__ServerMerchant(server_address)
        return cls.instance

    class __ServerMerchant(ServerAbstract):
        choices = [
            ServerAbstract.NEW_PRODUCT_OPERATION, 
            ServerAbstract.REMOVE_PRODUCT_OPERATION,
            ServerAbstract.NEW_SHOP_OPERATION,
            ServerAbstract.REMOVE_SHOP_OPERATION,
            ServerAbstract.LOGIN_OPERATION,
            ServerAbstract.REGISTER_OPERATION,
            ServerAbstract.LIST_SHOPS,
            ServerAbstract.LIST_PRODUCTS,
            ServerAbstract.CLOSE_OPERATION,
        ]

        def __init__(self, connect_address="localhost"):
            super(ServerMerchant.__ServerMerchant, self).__init__()
            self.port = 9002
            self.clientInstance = None

            self.server_address = connect_address
            self.server_port = 9000

            self.initialize()
            self.connectToMain()

        def connectToMain(self):
            self.clientInstance = ClientImpl()

            print("Connecting to server on %s:%s" % (self.server_address, self.server_port))
            self.clientInstance.initialize(self.server_address, self.server_port)

        def sendData(self, method, data):
            package = {
                "method": method,
                "data": data
            }

            packageToSend = json.dumps(package)
            self.clientInstance.send(packageToSend)

            return self.clientInstance.receive()

        def newShop(self, data):
            return self.sendData(self.NEW_SHOP_OPERATION, data)

        def newProduct(self, data):
            return self.sendData(self.NEW_PRODUCT_OPERATION, data)

        def removeProduct(self, data):
            return self.sendData(self.REMOVE_PRODUCT_OPERATION, data)

        def removeShop(self, data):
            return self.sendData(self.REMOVE_SHOP_OPERATION, data)

        def login(self, data):
            return self.sendData(self.LOGIN_OPERATION, data)

        def register(self, data):
            return self.sendData(self.REGISTER_OPERATION, data)

        def listShops(self, data):
            return self.sendData(self.LIST_SHOPS, data)

        def listProducts(self, data):
            return self.sendData(self.LIST_PRODUCTS, data)

        def closeConnection(self, data):
            raise CloseException()

        def performOperation(self, client, clientMessage):
            clientData = self.parseClientMessage(clientMessage)
            result = self.switchOperation(clientData)

            client.send(result)

        def switchOperation(self, clientData):
            switch = {
                self.NEW_SHOP_OPERATION: self.newShop,
                self.NEW_PRODUCT_OPERATION: self.newProduct,
                self.REMOVE_SHOP_OPERATION: self.removeShop,
                self.REMOVE_PRODUCT_OPERATION: self.removeProduct,
                self.LOGIN_OPERATION: self.login,
                self.REGISTER_OPERATION: self.register,
                self.LIST_SHOPS: self.listShops,
                self.LIST_PRODUCTS: self.listProducts,
                self.CLOSE_OPERATION: self.closeConnection
            }

            func = switch.get(clientData["method"], lambda: "Operazione non valida")
            return func(clientData["data"])