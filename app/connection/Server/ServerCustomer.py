from app.socket.Server.ServerAbstract import ServerAbstract
from app.socket.Client.ClientImpl import ClientImpl
from .CloseException import CloseException

import json

class ServerCustomer(object):
    instance = None

    @classmethod
    def getInstance(cls, server_address="localhost"):
        if (cls.instance is not None):
            return cls.instance
        
        cls.instance = ServerCustomer.__ServerCustomer(server_address)
        return cls.instance

    class __ServerCustomer(ServerAbstract):
        choices = [
            ServerAbstract.SEARCH_SHOP_OPERATION,
            ServerAbstract.SEARCH_PRODUCT_OPERATION,
            ServerAbstract.LIST_SHOPS,
            ServerAbstract.LIST_PRODUCTS,
            ServerAbstract.CLOSE_OPERATION
        ]

        def __init__(self, connect_address="localhost"):
            super(ServerCustomer.__ServerCustomer, self).__init__()

            self.port = 9003
            self.clientInstance = None

            self.server_address = connect_address
            self.server_port = 9000

            self.initialize()
            self.connectToMain()

        def connectToMain(self):
            self.clientInstance = ClientImpl()

            print("Connecting to server on %s:%s" % (self.server_address, self.server_port))
            self.clientInstance.initialize(self.server_address, self.server_port)

        def retrieveData(self, method, data):
            package = {
                "method": method,
                "data": data
            }

            packageToSend = json.dumps(package)
            self.clientInstance.send(packageToSend)
            return self.clientInstance.receive()

        def searchShop(self, data):
            return self.retrieveData(self.SEARCH_SHOP_OPERATION, data)

        def searchProduct(self, data):
            return self.retrieveData(self.SEARCH_PRODUCT_OPERATION, data)

        def listShops(self, data):
            return self.retrieveData(self.LIST_SHOPS, data)

        def closeConnection(self, data):
            raise CloseException

        def performOperation(self, client, clientMessage):
            clientData = self.parseClientMessage(clientMessage)
            result = self.switchOperation(clientData)

            client.send(result)

        def switchOperation(self, clientData):
            switch = {
                self.SEARCH_SHOP_OPERATION: self.searchShop,
                self.SEARCH_PRODUCT_OPERATION: self.searchProduct,
                self.LIST_SHOPS: self.listShops,
                self.CLOSE_OPERATION: self.closeConnection
            }

            func = switch.get(clientData["method"], lambda: "Operazione non valida")
            return func(clientData["data"])