import os

class FileHelper(object):
    @staticmethod
    def createFileIfNotExists(fileName):
        if (not FileHelper.fileExists(fileName)):
            with open(fileName, "w"): pass

    def fileExists(filename):
        return os.path.exists(filename)