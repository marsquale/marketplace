from app.connection.Server.ServerCustomer import ServerCustomer
from app.connection.Server.Threads.CustomerManager import CustomerManager
from threading import Thread, Lock

import sys

server_address = sys.argv[1] if len(sys.argv) > 1 else "localhost"

try:
    server = ServerCustomer.getInstance(server_address)
except Exception as e:
    print("Errore creazione server: %s" % str(e))
    exit(1)

mutex = Lock()
while True:
    client = server.accept()
    thread = CustomerManager(client, mutex)
    thread.start()

server.close()
